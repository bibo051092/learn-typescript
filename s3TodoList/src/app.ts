const API_URL = 'http://localhost:3000/todos';
const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const tbodyElement = ($('table tbody') as HTMLTableElement);
const nameInputValue = ($('input[name="name"]') as HTMLInputElement);
const saveBtn = ($('.btn-save') as HTMLButtonElement);

interface Todo {
    id?: number;
    name: string;
    isCompleted: boolean;
}

const todoApp = {
    async execute(): Promise<void> {
        await this.render();

        this.handleEvents();
    },
    async render(): Promise<void> {
        const todos = await this.getTodos();
        const html = todos.map((todo, index) => {
            return `
                 <tr>
                    <th scope="row">${todo.id}</th>
                    <td>${todo.name}</td>
                    <td>
                        ${(todo.isCompleted) ? 'Completed' : 'In progress'}
                    </td>
                    <td colspan="2">
                        <button class="btn btn-danger btn-delete" data-index="${index}">Delete</button>
                        ${(!todo.isCompleted) ? `<button class="btn btn-success btn-finished ms-1" data-index="${index}">Finished</button>` : ''}  
                    </td>
                </tr>
            `;
        });

        tbodyElement.innerHTML = html.join('');

        $$('.btn-delete').forEach((btn) => {
            btn.addEventListener('click', todoApp.deleteTodo);
        });

        $$('.btn-finished').forEach((btn) => {
            btn.addEventListener('click', todoApp.finishedTodo);
        });
    },
    handleEvents(): void {
        saveBtn.addEventListener('click', async (e) => {
            e.preventDefault();
            if (nameInputValue.value.trim() === '') return;

            const newTodo: Todo = {
                name: nameInputValue.value.trim(),
                isCompleted: false,
            };

            await todoApp.createTodo(newTodo);
            await todoApp.render();
            nameInputValue.value = '';
        });
    },
    async getTodos(): Promise<Todo[]> {
        const response = await fetch(API_URL);

        if (!response.ok) {
            throw new Error('Không thể tải dữ liệu từ API.');
        }

        return response.json();
    },
    async createTodo(todo: Todo): Promise<Todo> {
        const response: Response = await fetch(API_URL, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify(todo)
        });

        if (!response.ok) {
            throw new Error('Đăng ký không thành công.');
        }

        return response.json();
    },
    async deleteTodo(event: Event): Promise<void> {
        const index: number = parseInt(<string>(event.target as HTMLButtonElement).dataset.index);
        const todos = await todoApp.getTodos();
        const response: Response = await fetch(API_URL + `/${todos[index].id}`, {
            method: 'DELETE'
        });

        if (!response.ok) {
            throw new Error('Xóa todo không thành công.');
        }

        await todoApp.render();
    },
    async finishedTodo(event: Event): Promise<void> {
        const index: number = parseInt(<string>(event.target as HTMLButtonElement).dataset.index);
        const todos: Array<Todo> = await todoApp.getTodos();
        const response: Response = await fetch(API_URL + `/${todos[index].id}`, {
            method: 'PATCH',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify({isCompleted: true}),
        });

        if (!response.ok) {
            throw new Error('update todo không thành công.');
        }

        await todoApp.render();
    }
}

todoApp.execute().then();
