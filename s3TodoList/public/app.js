"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const API_URL = 'http://localhost:3000/todos';
const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);
const tbodyElement = $('table tbody');
const nameInputValue = $('input[name="name"]');
const saveBtn = $('.btn-save');
const todoApp = {
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.render();
            this.handleEvents();
        });
    },
    render() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const todos = yield this.getTodos();
                const html = todos.map((todo, index) => {
                    return `
                     <tr>
                        <th scope="row">${todo.id}</th>
                        <td>${todo.name}</td>
                        <td>
                            ${(todo.isCompleted) ? 'Completed' : 'In progress'}
                        </td>
                        <td colspan="2">
                            <button class="btn btn-danger btn-delete" data-index="${index}">Delete</button>
                            ${(!todo.isCompleted) ? `<button class="btn btn-success btn-finished ms-1" data-index="${index}">Finished</button>` : ''}  
                        </td>
                    </tr>
                `;
                });
                tbodyElement.innerHTML = html.join('');
                $$('.btn-delete').forEach((btn) => {
                    btn.addEventListener('click', todoApp.deleteTodo);
                });
                $$('.btn-finished').forEach((btn) => {
                    btn.addEventListener('click', todoApp.finishedTodo);
                });
            }
            catch (e) {
                console.log(e);
            }
        });
    },
    handleEvents() {
        saveBtn.addEventListener('click', (e) => __awaiter(this, void 0, void 0, function* () {
            e.preventDefault();
            if (nameInputValue.value.trim() === '')
                return;
            const newTodo = {
                name: nameInputValue.value.trim(),
                isCompleted: false,
            };
            yield todoApp.createTodo(newTodo);
            yield todoApp.render();
            nameInputValue.value = '';
        }));
    },
    getTodos() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield fetch(API_URL);
            if (!response.ok) {
                throw new Error('Không thể tải dữ liệu từ API.');
            }
            return response.json();
        });
    },
    createTodo(todo) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield fetch(API_URL, {
                method: 'POST',
                headers: {
                    'content-type': 'application/json',
                },
                body: JSON.stringify(todo)
            });
            if (!response.ok) {
                throw new Error('Đăng ký không thành công.');
            }
            return response.json();
        });
    },
    deleteTodo(event) {
        return __awaiter(this, void 0, void 0, function* () {
            const index = parseInt(event.target.dataset.index);
            const todos = yield todoApp.getTodos();
            const response = yield fetch(API_URL + `/${todos[index].id}`, {
                method: 'DELETE'
            });
            if (!response.ok) {
                throw new Error('Xóa todo không thành công.');
            }
            yield todoApp.render();
        });
    },
    finishedTodo(event) {
        return __awaiter(this, void 0, void 0, function* () {
            const index = parseInt(event.target.dataset.index);
            const todos = yield todoApp.getTodos();
            const response = yield fetch(API_URL + `/${todos[index].id}`, {
                method: 'PATCH',
                headers: {
                    'content-type': 'application/json',
                },
                body: JSON.stringify({ isCompleted: true }),
            });
            if (!response.ok) {
                throw new Error('update todo không thành công.');
            }
            yield todoApp.render();
        });
    }
};
todoApp.execute().then();
