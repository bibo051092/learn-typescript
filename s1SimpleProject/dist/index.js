"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const readlineSync = require("readline-sync");
const tasks = [];
function addTask(taskName) {
    const newTask = {
        id: tasks.length + 1,
        name: taskName,
        completed: false,
    };
    tasks.push(newTask);
}
function markTaskAsCompleted(taskId) {
    const task = tasks.find(task => task.id === taskId);
    if (task) {
        task.completed = true;
    }
}
function displayTasks() {
    console.log("\nTodo List:");
    tasks.forEach(task => {
        const status = task.completed ? "[x]" : "[ ]";
        console.log(`${status} ${task.name}`);
    });
}
function main() {
    console.log("Welcome to Todo List App!");
    while (true) {
        console.log("\n1. Add Task\n2. Mark Task as Completed\n3. Display Tasks\n4. Exit");
        const choice = readlineSync.question("Enter your choice: ");
        switch (choice) {
            case "1":
                const taskName = readlineSync.question("Enter task name: ");
                addTask(taskName);
                break;
            case "2":
                const taskId = parseInt(readlineSync.question("Enter task ID: "));
                markTaskAsCompleted(taskId);
                break;
            case "3":
                displayTasks();
                break;
            case "4":
                console.log("Goodbye!");
                process.exit(0);
            default:
                console.log("Invalid choice. Please try again.");
        }
    }
}
main();
